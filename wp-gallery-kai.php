<?php
/*
Plugin Name: wp-gallery-kai
Plugin URI: https://hansendo.com/
Description: ギャラリー表示を素敵な感じにするプラグイン。
Author: Hansendo D3 Studio
Version: 0.2.2
Author URI: https://hansendo.com/
*/

add_action('wp_head', 'add_head_scripts_wpgallerykai', 1);

if(!function_exists('add_head_scripts_wpgallerykai')) {
	function add_head_scripts_wpgallerykai() {
		global $wp_query;
		
		$flag = false;
		if($wp_query->posts) {
			for($i=0;$i<count($wp_query->posts);$i++) {
				if ( preg_match('/\[gallery([^\]]+)?\]/', $wp_query->posts[$i]->post_content) || preg_match('/<a\s.*?rel\s*=\s*(?:"|\')?lightbox(?:"|\')?[^>]*>/',$wp_query->posts[$i]->post_content) ) {
					$flag = true;
					break;
				}
			}
		}
		if(!is_admin() && $flag) {
			echo '<link href="'.get_site_url().'/wp-content/plugins/wp-gallery-kai/styles/nyroModal.css" media="screen" rel="stylesheet" type="text/css" />'."\n";
			wp_enqueue_script( 'jquery');
			wp_enqueue_script('nyroModal-1.3.1', '/wp-content/plugins/wp-gallery-kai/js/jquery.nyroModal-1.3.1.pack.js', array('jquery'));
			wp_enqueue_script('nyroModal-setting', '/wp-content/plugins/wp-gallery-kai/js/jquery.nyroModal-setting.js', array('jquery'));
			wp_enqueue_script('reflection', '/wp-content/plugins/wp-gallery-kai/js/reflection.js', array('jquery'));
		}
	}
}

function filter_wp_get_attachment_link_gallerykai($tag)
{
	$options = "<a rel='gal" . get_the_ID() . "' class='nyroModal' href=";
	$tag = str_replace('<a href=', $options, $tag);
	return $tag;
}
add_filter ('wp_get_attachment_link', 'filter_wp_get_attachment_link_gallerykai');

function filter_wp_get_attachment_image_gallerykai($attr)
{
	$attr['class'] = $attr['class']. " reflect";
	return $attr;
}
add_filter('wp_get_attachment_image_attributes', 'filter_wp_get_attachment_image_gallerykai');

function gallery_shortcode_kai($attr)
{
	$attr['link'] = 'file';
	return gallery_shortcode($attr);
}
add_shortcode('gallery', 'gallery_shortcode_kai');
?>